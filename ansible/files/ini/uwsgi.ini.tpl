[uwsgi]
socket = 127.0.0.1:3031
chdir = {{ project_root }}/code
pp = {{ project_root }}/code
virtualenv = {{ project_home }}/.virtualenv/{{ project_name }}
module = wsgi:application
processes = 4
threads = 2
stats = 127.0.0.1:9191