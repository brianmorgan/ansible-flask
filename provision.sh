#!/bin/sh

if [ $# -eq 1 ]; then
    ansible-playbook -i ansible/hosts ansible/provision.yml --limit=$1
else
    echo 'ERROR: please specify "vagrant" or "production"'
fi
