from flask import Blueprint, render_template

from . import route, login_required

bp = Blueprint('dashboard', __name__)

@bp.route('/')
@login_required
def index():
    """Returns the dashboard interface."""
    return render_template('dashboard.html')
