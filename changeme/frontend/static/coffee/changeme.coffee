Behavior2.Class('formfill', 'form', {
    reset: {
        'form': 'reset'
    }
}, ($ctx, that) ->
    that.reset = (evt) ->
        $ctx.find('button[type="submit"]').button('reset')
        $ctx.values($ctx.data('vars'))

    $ctx.values($ctx.data('vars'))
    $ctx.errors($ctx.data('errors'))
    $ctx.data('initialized', true)
    $ctx.trigger('initialized')
)
