# -*- coding: utf-8 -*-
"""
    tests.api
    ~~~~~~~~~

    api tests package
"""

from changeme.api import create_app

from .. import ChangemeAppTestCase, settings


class ChangemeApiTestCase(ChangemeAppTestCase):

    def _create_app(self):
        return create_app(settings, register_security_blueprint=True)

    def setUp(self):
        super(ChangemeApiTestCase, self).setUp()
        self._login()
